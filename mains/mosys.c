/* Copyright 2020 The ChromiumOS Authors
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

#include "mosys/cli.h"

int main(int argc, char *argv[])
{
	return mosys_main(argc, argv);
}
