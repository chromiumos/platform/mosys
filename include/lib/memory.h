/* Copyright 2019 The ChromiumOS Authors
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

#ifndef MOSYS_LIB_MEMORY_H__
#define MOSYS_LIB_MEMORY_H__

struct memory_cb;

extern struct memory_cb smbios_memory_cb;

#endif /* MOSYS_LIB_MEMORY_H__ */
