/* SPDX-License-Identifier: BSD-3-Clause */
/*
 * Copyright 2021 Google LLC
 *
 * SPD-like memory info for memory without real SPD data.
 */

#ifndef LIB_NONSPD_MODULES_H__
#define LIB_NONSPD_MODULES_H__

#include "lib/nonspd.h"

/*
 * Modules defined in lib/spd/nonspd_modules.c
 */
extern const struct nonspd_mem_info hynix_lpddr3_h9ccnnn8gtmlar_nud;
extern const struct nonspd_mem_info hynix_lpddr3_h9ccnnnbjtalar_nud;
extern const struct nonspd_mem_info hynix_lpddr4x_h9hcnnnbkmmlxr_nee;
extern const struct nonspd_mem_info hynix_lpddr4x_h9hcnnncpmalhr_nee;
extern const struct nonspd_mem_info hynix_lpddr4x_h9hcnnncpmmlxr_nee;
extern const struct nonspd_mem_info hynix_lpddr4x_h9hcnnnfammlxr_nee;
extern const struct nonspd_mem_info hynix_lpddr4x_h54g46cyrbx267;
extern const struct nonspd_mem_info hynix_lpddr4x_h54g56cyrbx247;
extern const struct nonspd_mem_info hynix_lpddr4x_h54g56cyrbx247n;
extern const struct nonspd_mem_info hynix_lpddr4x_h54g68cyrbx248;
extern const struct nonspd_mem_info hynix_lpddr4x_h54g68cyrbx248n;
extern const struct nonspd_mem_info hynix_lpddr3_h9ccnnnbltblar_nud;
extern const struct nonspd_mem_info micron_lpddr3_mt52l256m32d1pf_10;
extern const struct nonspd_mem_info micron_lpddr3_mt52l256m32d1pf_107wtb;
extern const struct nonspd_mem_info micron_lpddr3_mt52l512m32d2pf_107wtb;
extern const struct nonspd_mem_info micron_lpddr3_mt52l512m32d2pf_10;
extern const struct nonspd_mem_info micron_lpddr4x_mt29vzzzad8dqksl;
extern const struct nonspd_mem_info micron_lpddr4x_mt29vzzzad8gqfsl_046;
extern const struct nonspd_mem_info micron_lpddr4x_mt29vzzzbd9dqkpr_046;
extern const struct nonspd_mem_info micron_lpddr4x_mt29vzzzad9gqfsm_046;
extern const struct nonspd_mem_info micron_lpddr4x_mt29vzzzcd9gqkpr_046;
extern const struct nonspd_mem_info micron_lpddr4x_mt53e1g32d4nq_046wte;
extern const struct nonspd_mem_info micron_lpddr4x_mt53e1g32d2np_046wta;
extern const struct nonspd_mem_info micron_lpddr4x_mt53e1g32d2np_046wtb;
extern const struct nonspd_mem_info micron_lpddr4x_mt53e2g32d4nq_046wta;
extern const struct nonspd_mem_info micron_lpddr4x_mt53e2g32d4nq_046wtc;
extern const struct nonspd_mem_info micron_lpddr4x_mt53e512m32d1np_046wtb;
extern const struct nonspd_mem_info micron_lpddr4x_mt53e512m32d2np_046;
extern const struct nonspd_mem_info micron_lpddr4x_mt53d1g32d4dt_046wtd;
extern const struct nonspd_mem_info nanya_lpddr3_nt6cl512t32am_h0;
extern const struct nonspd_mem_info samsung_lpddr3_k4e6e304ee_egce;
extern const struct nonspd_mem_info samsung_lpddr3_k4e6e304eb_egcf;
extern const struct nonspd_mem_info samsung_lpddr3_k4e6e304ec_egcg;
extern const struct nonspd_mem_info samsung_lpddr3_k4e8e304ee_egce;
extern const struct nonspd_mem_info samsung_lpddr3_k4e8e324eb_egcf;
extern const struct nonspd_mem_info samsung_lpddr3_k4e6e304ed_egcg;
extern const struct nonspd_mem_info samsung_lpddr4x_kmdh6001da_b422;
extern const struct nonspd_mem_info samsung_lpddr4x_kmdp6001da_b425;
extern const struct nonspd_mem_info samsung_lpddr4x_kmdv6001da_b620;
extern const struct nonspd_mem_info samsung_lpddr4x_k4u6e3s4aa_mgcr;
extern const struct nonspd_mem_info samsung_lpddr4x_k4u6e3s4ab_mgcl;
extern const struct nonspd_mem_info samsung_lpddr4x_k4ube3d4aa_mgcl;
extern const struct nonspd_mem_info samsung_lpddr4x_k4ube3d4ab_mgcl;
extern const struct nonspd_mem_info samsung_lpddr4x_k4ube3d4aa_mgcr;
extern const struct nonspd_mem_info samsung_lpddr4x_k4uce3d4aa_mgcl;
extern const struct nonspd_mem_info samsung_lpddr4x_k4uce3q4aa_mgcr;
extern const struct nonspd_mem_info samsung_lpddr4x_k4uce3q4ab_mgcl;
extern const struct nonspd_mem_info sandisk_lpddr4x_sdada4cr_128g;
extern const struct nonspd_mem_info foresee_lpddr4x_feprf6432_58a1930;
extern const struct nonspd_mem_info foresee_lpddr4x_flxc2004g_n1;
extern const struct nonspd_mem_info cxmt_lpddr4x_cxdb5ccbm_ml_a;
extern const struct nonspd_mem_info nanya_lpddr4x_nt6ap1024f32bl_j1;

#endif /* LIB_NONSPD_MODULES_H__ */
