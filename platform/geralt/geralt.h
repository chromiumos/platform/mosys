/* Copyright 2023 The ChromiumOS Authors
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

#ifndef PLATFORM_GERALT_H
#define PLATFORM_GERALT_H

#include "mosys/platform.h"

/* platform callbacks */
extern struct memory_cb geralt_memory_cb;

#endif /* PLATFORM_GERALT_H */
