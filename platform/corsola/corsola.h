/* Copyright 2022 The ChromiumOS Authors
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

#ifndef PLATFORM_CORSOLA_H
#define PLATFORM_CORSOLA_H

#include "mosys/platform.h"

/* platform callbacks */
extern struct memory_cb corsola_memory_cb;

#endif /* PLATFORM_CORSOLA_H */
