/* Copyright 2024 The ChromiumOS Authors
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

#ifndef PLATFORM_ARM_H
#define PLATFORM_ARM_H

#include "mosys/platform.h"

/* platform callbacks */
extern struct memory_cb arm_memory_cb;

#endif /* PLATFORM_ARM_H */
