/* Copyright 2021 The ChromiumOS Authors
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

#ifndef PLATFORM_CHERRY_H
#define PLATFORM_CHERRY_H

#include "mosys/platform.h"

/* platform callbacks */
extern struct memory_cb cherry_memory_cb;

#endif /* PLATFORM_CHERRY_H */
